\documentclass[12pt]{article}

\usepackage{natbib}
\usepackage{setspace}
\usepackage[left=1in, right=1in, top=1in, bottom=1in]{geometry}

\doublespacing

\author{Brian To}
\title{Writing Authority}
\date{Spring 2014}

\begin{document}
  \maketitle

When we were first introduced to the subject of writing, we were taught how to
write academically. That is, we were taught how to write in the academic
discourse. In this discourse, we are expected to follow certain rules (or
perhaps, guidelines) if we want our papers to be judged ``good.'' For example,
if a student were to write a paper arguing the social stratification present in
\emph{The Great Gatsby}, then he or she must validate his or her argument by
supporting it with facts. He or she must directly quote from the novel and
explain how it contributes to his or her thesis. However, this is only one of
many styles of writing. Not every instance of writing is academic. When
journalists write magazine and newspaper articles, they support their facts with
quotes and evidence, but present the information with a different color and the writing is
less directed towards proving a point. Similarly, when engineers and other
technical writers write research papers, engineers use the academic discourse as a
base, but like the journalists, emphasize different points and value different
criteria. These three discourses: academic, journalistic, and technical have
different criteria for what constitutes good writing. In other words, these three
discourses have different values for what is \emph{authoritative} writing. So, what is
\emph{authoritative} writing, and how can we improve the academic discourse with
the authoritative criteria of the other discourses?

Let's take a look at what makes a paper authoritative from an academic
standpoint. One way to interpret authority is the credibility and quality of a paper's
cited sources, a view point stressed by high school English teachers. When one uses a
credible and legitimate source,
his or her argument is strengthened by the legitimacy of not only the reasoning
of others, but the reputation of the people he or she cites. During an interview
with Professor Daniel Krutz of the RIT Software Engineering department, one
criteria he looks for when finding a paper to cite is the reputation of the publishing
conference. Conferences have different quality thresholds for submitting papers.
For example, the IEEE and ACM are two
conferences that are known for their strict admittance criteria. If a paper were
to be published from the IEEE, it is almost guaranteed that the paper is of high
quality\citep{krutz}. In contrast, if one were to create his or her own conference,
it would not have the credibility and pedigree of more established conferences.
There would be no indicator of quality.

Likewise, the number of citations as a function of the publication date of a paper is
an indicator to the authority of an essay. The more citations from other
respected authors a paper has, the more authoritative the paper is. An older
paper (such as \emph{No Silver Bullet} by Fred Brooks in 1986) with over 3,800 citations
is more credible than an older paper with few citations. However, an
old paper that is rarely cited indicates that the paper was not very
influential. Also, more recent papers provide more current valuable
knowledge in a given field\citep{krutz}. An old paper's wisdom is already known;
a new paper's knowledge is truly new. Usage of new information indicates
authority.

Another signal or indicator of authority is how the author responds to
conflicting statements between papers and how he or she disagrees with an
author. Authority is the ability to evaluate claims by their merit and assert
that no single claim is completely incorrect. In the case study conducted by
Penrose and Geisler, they studied the writing style of a college freshman and a
doctorate in philosophy. When Janet, the college freshman, encounters a text
with conflicting information, she takes a single side for the sake of the paper,
disregarding her own conflicting internal opinions. However when Rodger, the
doctorate writer, encounters conflicting opinions in different papers, he ``used
such controversies as a springboard\ldots to develop his own
position.''\citep[pg. 608-609]{penrose} Penrose is not trying to insult or
belittle Janet. Rather, she is used to contrast the course of action of someone
who does not have writing authority versus some who does. A person with
authority will not blindly choose a side and support it. A person who writes
with authority will evaluate the different sides of an argument before
committing to an opinion. Even so, he or she might not pick a side per se, but
will pick out the insights and faults from both views and form his or her own
opinion on the matter.

In contrast, the journalistic discourse has different criteria for determining
authority. Richard Paul Jackson writes that journalistic authority is
``epistemic in nature; it is anchored in professional standards; it is expressed
through conventions of journalistic practice; \ldots and is dependent to some
degree on the shared understandings with the readers and/or viewers.''\citep[pg.
8]{jackson} In journalism, it is not only about what facts you present, but
where the facts came from and how you present those facts to your readers.

Jackson draws from various other authors in the realm of journalism to help
shape and clarify his definition of journalistic authority. One interpretation
of authority by Don Fry and Joshua Meyrowitz is credibility and the power to
``coerce'' an audience to believe in the writing\citep[pg. 9]{jackson}. This
means that an authoritative journalist is one who can convince readers to
believe in a certain viewpoint via epistemic channels. In relation to academic
writing, I believe that the ability to coerce or persuade is important. However,
it is not a factor in authority. In academic writing, it is possible to be
convincing without having authority in a subject. For example, a writer may be
able to convince a naive audience that technology is evil or communism is good.
That writer doesn't necessarily need authority in technology or communism. He or
she needs public appeal. That doesn't mean that authoritative writing is not
credible. A writer who holds authority in a subject but does not write
convincingly (such as in convoluted arguments) will produce writing that does
not \emph{sound} authoritative and therefore is not authoritative.

Furthermore, another way of looking at authority in journalism is objectivity. An
objective journalist is an authoritative journalist. Jackson cites Schudson that
``objectivity's normative power rests in it's `faith in facts,' a distrust of
`values,' and a commitment to their segregation.''\citep[pg. 10-11]{jackson}
However, Jackson notes that objectivity is a difficult, nearly unattainable
ideal and journalists question if objectivity is important at all to the
culture\citep[pg. 11]{jackson}. From an academic standpoint, objectivity is
less valuable than in journalism. Objectivity is viewed as a guideline in academic
writing. Writers should ideally support arguments with facts from credible
sources. However, it is inevitable that values sneak into a writer's text. An
authoritative piece of writing is directed and has a thesis or point, whether
indirect or direct. By that definition, academic papers must have values and
therefore cannot be completely objective. Writers may counter and build
off facts from others, but a thesis will always exist. Much like journalistic
writing, objectivity is an indicator for authoritative academic writing, but is not
strictly necessary.

There are many other ways to qualify authority in journalism too numerous to
list here. However, I shall discuss one more. Jackson references Schudson who
states that authority in journalism relies on the relationship between the
audience and the professional image of the journalist and company, and the
balance between that relation and the factual correctness of their
work\citep[pg. 11-12]{jackson}. In academic writing, the relation between the
reader and author is more transient. Writers do not need to maintain
a relation as close to readers that journalists, but both discourses place value in the
correctness of what they write. Academic writers may take liberties and deviate from
``political correctness,'' but journalists must be more cautious. When they
write, they must not only represent themselves, but also those who employed them
and placed trust in their writing. If a journalist writes distasteful messages
or is not politically correct, it reflects badly on both the journalist and
company. Writers have no such benefactors. In academic writing, the writer's
relationship with his or her audience is a minimal factor in determining a writer's
authority. In contrast, factual correctness is emphasized in both discourses and
is an important factor in authority.

Just as journalism has different factors from academic writing that
impact a writer's authority, so
does the technical (engineering) writing discourse. In Summer Louise Smith's
thesis on comparing writing and engineering teachers' take on a paper, she notes
differences in how writing and engineering teachers grade sample papers on
engineering topics. Even though Smith herself rarely touches on the topic of
authority, I will use each side's grading criteria as a proxy for writing
authority.

Smith notes that ``engineering teachers are 4.75\% more likely to state an
evaluation of validity than writing teachers''\citep[pg. 44]{smith} and that
``writing teachers are slightly more likely to state evaluations of coherence,
organization, and design.''\citep[pg. 45]{smith} Note that Smith is \emph{not}
saying that engineering teachers do not focus on coherence and structure nor
that writing teachers neglect validity. Smith is saying that there is a slight
emphasis difference between the discourses. In academic writing, both validity
and form are important. In relation to authority, I believe that ``coherence,
organization, and design'' will help a paper \emph{sound} authoritative.
Validity in details will help support a paper and provide it authority. However,
a paper written in bad form will not have the air of authority that a paper
with decent ``coherence, organization, and design'' will. If a paper does not
sound authoritative, readers will be less likely to perceive it as
authoritative.

Different writing discourses hold different but logical, and in ways similar,
values to academic writing. In academic writing, authority includes the quality and
age of referenced sources and the ability to gracefully handle disagreements in
sources. In journalistic writing, authority is the ability to persuade others,
to be objective, and to balance facts with connecting to your reader. In
technical writing, authority is both factual validity and writing coherence.

\newpage

\bibliographystyle{mla-good}
\bibliography{essay}

\end{document}
